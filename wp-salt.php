<?php
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 */
define('AUTH_KEY',         'QazQGDUS9oey7aH2eeyQzXMSxne3tuE5X3hM5wLovczbHbvqjIBJbFzBHMASRS9N');
define('SECURE_AUTH_KEY',  'FPM2mwM9gxbTTWbo8hp030zDEvI6GDop8ffo9HC9Me1j4WIFLS3hqeKci2wrmHqU');
define('LOGGED_IN_KEY',    'yJaUpVnFEYqMV5sVcXJgFiGjM0djEDwQgMiRnEqEq75eftE8bMyHWYQyUcTqPWD7');
define('NONCE_KEY',        'jS3nIWYWgyKbJUR2vNFYoSp611dHWQX6wseQSo0BTFUKKWniU15XngpzLwHKPgfD');
define('AUTH_SALT',        'yybqqT9tM2qgYVajgYjfHcEdBLPP8Tu5sbeCFjbft6u2xA3ptaYuVmPNI0uCbLQz');
define('SECURE_AUTH_SALT', 'UpgYw33mmKroUs59mFeCexDgXdPXcze8Nw5Nmyos6Wmmfc7U40xfJ72H4RUsfg8j');
define('LOGGED_IN_SALT',   'oGJJL1trhc3gxHFnsaoPRxF3GNwFxsLXAQmjL43jgEYmiKFRyKe6Frbx2VBsuQti');
define('NONCE_SALT',       'aqrTUh6f2eEqQo7u2tGoJ7s85pQ2avjcufUtafFAX77UI6o4ARmN27DCshGIRNyh');
