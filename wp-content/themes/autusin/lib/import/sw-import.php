<?php 
function sw_import_files() { 
	return array(

		array(
			'import_file_name'             => 'Home',
			'page_title'				   => 'Home',
			'local_import_file'            => trailingslashit( get_template_directory() ) . 'lib/import/demo-1/data.xml',
			'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'lib/import/demo-1/widgets.json',
			'local_import_revslider'  		 => array( 
				'slide1' => trailingslashit( get_template_directory() ) . 'lib/import/demo-1/slideshow1.zip',
				),
			'local_import_options'         => array(
				array(
					'file_path'   => trailingslashit( get_template_directory() ) . 'lib/import/demo-1/theme_options.txt',
					'option_name' => 'autusin_theme',
					),
				),
			'menu_locate'									 => array(
				'primary_menu' => 'Primary Menu',
				'vertical_menu' => 'Vertical Menu',
				'mobile_menu1' => 'Mobile Menu'
				),
			'import_preview_image_url'     => get_template_directory_uri() . '/lib/import/demo-1/1.jpg',
			'import_notice'                => __( 'After you import this demo, you will have to setup the slider separately. This import maybe finish on 10-15 minutes', 'autusin' ),
			'preview_url'                  => esc_url( 'http://demo.wpthemego.com/themes/sw_autusin/' ),
			),
		array(
			'import_file_name'             => 'Home Page 2',
			'page_title'				   => 'Home Page 2',
			'local_import_file'            => trailingslashit( get_template_directory() ) . 'lib/import/demo-1/data.xml',
			'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'lib/import/demo-1/widgets.json',
			'local_import_revslider'  		 => array( 
				'slide1' => trailingslashit( get_template_directory() ) . 'lib/import/demo-2/slideshow1.zip',
				),
			'local_import_options'         => array(
				array(
					'file_path'   => trailingslashit( get_template_directory() ) . 'lib/import/demo-2/theme_options.txt',
					'option_name' => 'autusin_theme',
					),
				),
			'menu_locate'									 => array(
				'primary_menu' => 'Primary Menu',
				'vertical_menu' => 'Vertical Menu',
				'mobile_menu1' => 'Mobile Menu'
				),
			'import_preview_image_url'     => get_template_directory_uri() . '/lib/import/demo-2/2.jpg',
			'import_notice'                => __( 'After you import this demo, you will have to setup the slider separately. This import maybe finish on 10-15 minutes', 'autusin' ),
			'preview_url'                  => esc_url( 'http://demo.wpthemego.com/themes/sw_autusin/home-page-2' ),
			),

		array(
			'import_file_name'             => 'Home Page 3',
			'page_title'				   => 'Home Page 3',
			'local_import_file'            => trailingslashit( get_template_directory() ) . 'lib/import/demo-3/data.xml',
			'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'lib/import/demo-3/widgets.json',
			'local_import_revslider'  		 => array( 
				'slide1' => trailingslashit( get_template_directory() ) . 'lib/import/demo-3/slideshow3.zip',
				),
			'local_import_options'         => array(
				array(
					'file_path'   => trailingslashit( get_template_directory() ) . 'lib/import/demo-3/theme_options.txt',
					'option_name' => 'autusin_theme',
					),
				),
			'menu_locate'									 => array(
				'primary_menu' => 'Primary Menu',
				'vertical_menu' => 'Vertical Menu',
				'mobile_menu1' => 'Mobile Menu'
				),
			'import_preview_image_url'     => get_template_directory_uri() . '/lib/import/demo-3/3.jpg',
			'import_notice'                => __( 'After you import this demo, you will have to setup the slider separately. This import maybe finish on 10-15 minutes', 'autusin' ),
			'preview_url'                  => esc_url( 'http://demo.wpthemego.com/themes/sw_autusin/demo2' ),
			),
		array(
			'import_file_name'             => 'Home Page 4',
			'page_title'				   => 'Home Page 4',
			'local_import_file'            => trailingslashit( get_template_directory() ) . 'lib/import/demo-3/data.xml',
			'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'lib/import/demo-3/widgets.json',
			'local_import_revslider'  		 => array( 
				'slide1' => trailingslashit( get_template_directory() ) . 'lib/import/demo-4/slideshow4.zip',
				),
			'local_import_options'         => array(
				array(
					'file_path'   => trailingslashit( get_template_directory() ) . 'lib/import/demo-4/theme_options.txt',
					'option_name' => 'autusin_theme',
					),
				),
			'menu_locate'									 => array(
				'primary_menu' => 'Primary Menu',
				'vertical_menu' => 'Vertical Menu',
				'mobile_menu1' => 'Mobile Menu'
				),
			'import_preview_image_url'     => get_template_directory_uri() . '/lib/import/demo-4/4.jpg',
			'import_notice'                => __( 'After you import this demo, you will have to setup the slider separately. This import maybe finish on 10-15 minutes', 'autusin' ),
			'preview_url'                  => esc_url( 'http://demo.wpthemego.com/themes/sw_autusin/demo2/home-page-4' ),
			),

		array(
			'import_file_name'             => 'Home Page 5',
			'page_title'				   => 'Home Page 5',
			'local_import_file'            => trailingslashit( get_template_directory() ) . 'lib/import/demo-5/data.xml',
			'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'lib/import/demo-5/widgets.json',
			'local_import_revslider'  		 => array( 
				'slide1' => trailingslashit( get_template_directory() ) . 'lib/import/demo-5/slideshow5.zip',
				),
			'local_import_options'         => array(
				array(
					'file_path'   => trailingslashit( get_template_directory() ) . 'lib/import/demo-5/theme_options.txt',
					'option_name' => 'autusin_theme',
					),
				),
			'menu_locate'									 => array(
				'primary_menu' => 'Primary Menu',
				'vertical_menu' => 'Vertical Menu',
				'mobile_menu1' => 'Mobile Menu'
				),
			'import_preview_image_url'     => get_template_directory_uri() . '/lib/import/demo-5/5.jpg',
			'import_notice'                => __( 'After you import this demo, you will have to setup the slider separately. This import maybe finish on 10-15 minutes', 'autusin' ),
			'preview_url'                  => esc_url( 'http://demo.wpthemego.com/themes/sw_autusin/demo3' ),
			),
		array(
			'import_file_name'             => 'Home Page 6',
			'page_title'				   => 'Home Page 6',
			'local_import_file'            => trailingslashit( get_template_directory() ) . 'lib/import/demo-5/data.xml',
			'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'lib/import/demo-5/widgets.json',
			'local_import_revslider'  		 => array( 
				'slide1' => trailingslashit( get_template_directory() ) . 'lib/import/demo-6/slideshow6.zip',
				),
			'local_import_options'         => array(
				array(
					'file_path'   => trailingslashit( get_template_directory() ) . 'lib/import/demo-6/theme_options.txt',
					'option_name' => 'autusin_theme',
					),
				),
			'menu_locate'									 => array(
				'primary_menu' => 'Primary Menu',
				'vertical_menu' => 'Vertical Menu',
				'mobile_menu1' => 'Mobile Menu'
				),
			'import_preview_image_url'     => get_template_directory_uri() . '/lib/import/demo-6/6.jpg',
			'import_notice'                => __( 'After you import this demo, you will have to setup the slider separately. This import maybe finish on 10-15 minutes', 'autusin' ),
			'preview_url'                  => esc_url( 'http://demo.wpthemego.com/themes/sw_autusin/demo3/home-page-6' ),
			),

		array(
			'import_file_name'             => 'Home Page 7',
			'page_title'				   => 'Home Page 7',
			'local_import_file'            => trailingslashit( get_template_directory() ) . 'lib/import/demo-7/data.xml',
			'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'lib/import/demo-7/widgets.json',
			'local_import_revslider'  		 => array( 
				'slide1' => trailingslashit( get_template_directory() ) . 'lib/import/demo-7/slideshow7.zip',
				'slide2' => trailingslashit( get_template_directory() ) . 'lib/import/demo-7/slideshow7_1.zip',
				),
			'local_import_options'         => array(
				array(
					'file_path'   => trailingslashit( get_template_directory() ) . 'lib/import/demo-7/theme_options.txt',
					'option_name' => 'autusin_theme',
					),
				),
			'menu_locate'									 => array(
				'primary_menu' => 'Primary Menu',
				'vertical_menu' => 'Vertical Menu',
				'mobile_menu1' => 'Mobile Menu',
				'category_menu' => 'Category Menu Home7'
				),
			'import_preview_image_url'     => get_template_directory_uri() . '/lib/import/demo-7/7.jpg',
			'import_notice'                => __( 'After you import this demo, you will have to setup the slider separately. This import maybe finish on 10-15 minutes', 'autusin' ),
			'preview_url'                  => esc_url( 'http://demo.wpthemego.com/themes/sw_autusin/demo4/home-page-7' ),
			),
		array(
			'import_file_name'             => 'Home Page 8',
			'page_title'				   => 'Home Page 8',
			'local_import_file'            => trailingslashit( get_template_directory() ) . 'lib/import/demo-7/data.xml',
			'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'lib/import/demo-7/widgets.json',
			'local_import_revslider'  		 => array( 
				'slide1' => trailingslashit( get_template_directory() ) . 'lib/import/demo-8/slideshow8.zip',
				),
			'local_import_options'         => array(
				array(
					'file_path'   => trailingslashit( get_template_directory() ) . 'lib/import/demo-8/theme_options.txt',
					'option_name' => 'autusin_theme',
					),
				),
			'menu_locate'									 => array(
				'primary_menu' => 'Primary Menu',
				'vertical_menu' => 'Vertical Menu',
				'mobile_menu1' => 'Mobile Menu',
				),
			'import_preview_image_url'     => get_template_directory_uri() . '/lib/import/demo-8/8.jpg',
			'import_notice'                => __( 'After you import this demo, you will have to setup the slider separately. This import maybe finish on 10-15 minutes', 'autusin' ),
			'preview_url'                  => esc_url( 'http://demo.wpthemego.com/themes/sw_autusin/demo4/home-page-8' ),
			),
		array(
			'import_file_name'             => 'Home Page 9',
			'page_title'				   => 'Home Page 9',
			'local_import_file'            => trailingslashit( get_template_directory() ) . 'lib/import/demo-7/data.xml',
			'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'lib/import/demo-7/widgets.json',
			'local_import_revslider'  		 => array( 
				'slide1' => trailingslashit( get_template_directory() ) . 'lib/import/demo-9/slideshow9.zip',
				),
			'local_import_options'         => array(
				array(
					'file_path'   => trailingslashit( get_template_directory() ) . 'lib/import/demo-9/theme_options.txt',
					'option_name' => 'autusin_theme',
					),
				),
			'menu_locate'									 => array(
				'primary_menu' => 'Primary Menu',
				'vertical_menu' => 'Vertical Menu',
				'mobile_menu1' => 'Mobile Menu',
				),
			'import_preview_image_url'     => get_template_directory_uri() . '/lib/import/demo-9/9.jpg',
			'import_notice'                => __( 'After you import this demo, you will have to setup the slider separately. This import maybe finish on 10-15 minutes', 'autusin' ),
			'preview_url'                  => esc_url( 'http://demo.wpthemego.com/themes/sw_autusin/demo4/home-page-9' ),
			),
		array(
			'import_file_name'             => 'Home Page 10',
			'page_title'				   => 'Home Page 10',
			'local_import_file'            => trailingslashit( get_template_directory() ) . 'lib/import/demo-7/data.xml',
			'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'lib/import/demo-7/widgets.json',
			'local_import_revslider'  		 => array( 
				'slide1' => trailingslashit( get_template_directory() ) . 'lib/import/demo-10/slideshow10.zip',
				),
			'local_import_options'         => array(
				array(
					'file_path'   => trailingslashit( get_template_directory() ) . 'lib/import/demo-10/theme_options.txt',
					'option_name' => 'autusin_theme',
					),
				),
			'menu_locate'									 => array(
				'primary_menu' => 'Primary Menu',
				'vertical_menu' => 'Vertical Menu',
				'mobile_menu1' => 'Mobile Menu',
				),
			'import_preview_image_url'     => get_template_directory_uri() . '/lib/import/demo-10/10.jpg',
			'import_notice'                => __( 'After you import this demo, you will have to setup the slider separately. This import maybe finish on 10-15 minutes', 'autusin' ),
			'preview_url'                  => esc_url( 'http://demo.wpthemego.com/themes/sw_autusin/demo4/home-page-10' ),
			),

		array(
			'import_file_name'             => 'Home Page 11',
			'page_title'				   => 'Home Page 11',
			'local_import_file'            => trailingslashit( get_template_directory() ) . 'lib/import/demo-11/data.xml',
			'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'lib/import/demo-11/widgets.json',
			'local_import_revslider'  		 => array( 
				'slide1' => trailingslashit( get_template_directory() ) . 'lib/import/demo-11/slideshow11.zip',
				),
			'local_import_options'         => array(
				array(
					'file_path'   => trailingslashit( get_template_directory() ) . 'lib/import/demo-11/theme_options.txt',
					'option_name' => 'autusin_theme',
					),
				),
			'menu_locate'									 => array(
				'primary_menu' => 'Primary Menu',
				'vertical_menu' => 'Vertical Menu',
				'mobile_menu1' => 'Mobile Menu',
				),
			'import_preview_image_url'     => get_template_directory_uri() . '/lib/import/demo-11/11.jpg',
			'import_notice'                => __( 'After you import this demo, you will have to setup the slider separately. This import maybe finish on 10-15 minutes', 'autusin' ),
			'preview_url'                  => esc_url( 'http://demo.wpthemego.com/themes/sw_autusin/demo5/' ),
			),
		array(
			'import_file_name'             => 'Home Page 12',
			'page_title'				   => 'Home Page 12',
			'local_import_file'            => trailingslashit( get_template_directory() ) . 'lib/import/demo-11/data.xml',
			'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'lib/import/demo-11/widgets.json',
			'local_import_revslider'  		 => array( 
				'slide1' => trailingslashit( get_template_directory() ) . 'lib/import/demo-12/slideshow12.zip',
				),
			'local_import_options'         => array(
				array(
					'file_path'   => trailingslashit( get_template_directory() ) . 'lib/import/demo-12/theme_options.txt',
					'option_name' => 'autusin_theme',
					),
				),
			'menu_locate'									 => array(
				'primary_menu' => 'Primary Menu',
				'vertical_menu' => 'Vertical Menu',
				'mobile_menu1' => 'Mobile Menu',
				),
			'import_preview_image_url'     => get_template_directory_uri() . '/lib/import/demo-12/12.jpg',
			'import_notice'                => __( 'After you import this demo, you will have to setup the slider separately. This import maybe finish on 10-15 minutes', 'autusin' ),
			'preview_url'                  => esc_url( 'http://demo.wpthemego.com/themes/sw_autusin/demo5/home-page-12' ),
			),

	);
}
add_filter( 'pt-ocdi/import_files', 'sw_import_files' );